
# tunnel

A note on ssh tunnel setup.

## Setup

Enable `GatewayPorts yes` under the host's `sshd_config`. Then run

````
ssh -RNf 2222:localhost:22 user@guest
````

On the guest run

````
ssh -p 2222 localhost
````

If you want to have the tunnel automatically on startup consider using `autossh`. The easiest way is to create a ssh key with an empty passphrase and start / enable the following unit

````
[Unit]
Description=AutoSSH service for port 2222
After=network.target

[Service]
Environment="AUTOSSH_GATETIME=0"
ExecStart=/usr/bin/autossh -M 0 -o TCPKeepAlive=yes -o ControlMaster=no -RN 2222:localhost:22 user@guest

[Install]
WantedBy=multi-user.target
````
